import kivy
from kivy.core.window import Window
from kivy.app  import App
from kivy.lang import Builder
from kivy.uix.screenmanager import ScreenManager, Screen, FadeTransition
from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.properties import ObjectProperty
from kivy.config import Config
from kivy.uix.image import Image
from kivy.uix.label import Label
from kivy.clock import Clock
import cv2
import os
import detekcja
import czerwony
import time
kivy.require('1.11.1')


class SmartApp(App):
    def build(self):
        makeUI = Builder.load_file('smart.kv')
        return makeUI

class MainScreen(Screen):
    adressVar = ObjectProperty()
    filenameVar = ObjectProperty()
    wynik = 0
    ready = True
    def getImage(self, adress, name):
        os.system('curl {} > ./images/raw/raw.jpg'.format(adress, name))
        self.ids.rawimage.reload()
    def getNamefield(self):
        return self.filenameVar.text
    def loadImage(self):
        self.img = cv2.imread('./images/raw/raw.jpg')
        print('loading image')
        self.updateImage()
    def updateImage(self):
        #cv2.imshow('img', self.img)
        #cv2.waitKey(10000)
        cv2.imwrite('tmp.jpg', self.img)
        self.ids.imgcurrent.reload()
        print('updated image')
    def cropImage(self):
        self.img = self.img[int(self.ids.cropx1.text):int(self.ids.cropx2.text) ,int(self.ids.cropy1.text):int(self.ids.cropy2.text)]
        cv2.imwrite('./images/cropped/cropped.jpg', self.img)
        self.updateImage()
    def greyImage(self):
        self.img = cv2.cvtColor(self.img, cv2.COLOR_BGR2GRAY)
        cv2.imwrite('./images/grayscaled/grayscaled.jpg', self.img)
        self.updateImage()
    def blurImage(self):
        self.img = cv2.boxFilter(self.img, -1, (int(self.ids.blur1.text),int(self.ids.blur2.text)), (int(self.ids.blur3.text), int(self.ids.blur4.text)))
        cv2.imwrite('./images/blurred/blurred.jpg', self.img)
        self.updateImage()
    def filterDifferentiateImage(self):
        self.img = cv2.Scharr(self.img, -1, int(self.ids.diff1.text), int(self.ids.diff2.text), int(self.ids.diff3.text), int(self.ids.diff4.text))
        cv2.imwrite('./images/scharr/scharr.jpg', self.img)
        self.updateImage()
    def edgeDetectimage(self):
        self.img = cv2.Canny(self.img, int(self.ids.edge1.text), int(self.ids.edge2.text), int(self.ids.edge3.text))
        cv2.imwrite('./images/edgeDetected/edgeDetected.jpg', self.img)
        self.updateImage()
    def cornerDetectImage(self):
        self.img = cv2.cornerHarris(self.img,int(self.ids.corner1.text), int(self.ids.corner2.text), float(self.ids.corner3.text) )
        cv2.imwrite('./images/cornerDetected/cornerDetected.jpg', self.img)
        self.updateImage()
    def circleDetectImage(self):
        self.myBeta, self.myGamma = detekcja.kolko()
    def redDetectImage(self):
        self.myAlpha = czerwony.wskazowka()
        self.myZakresvar = 4
        self.myZakreskat = self.myGamma-self.myBeta

        self.myCzulosc =  self.myZakresvar/self.myZakreskat
        self.myWskazaniekat = self.myAlpha-self.myBeta
        self.wynik = self.myWskazaniekat*self.myCzulosc
        self.ids.wyswietlacz.text = str(self.wynik)
        print(self.myAlpha)
        print('===================alfa^==============')
        print(self.myBeta)
        print('===================beta^==============')
        print(self.myGamma)
        print('===================gamma^==============')
        print(self.wynik)
        print('===================Wynik^==============')

    def auto(self, adress, name):
        self.ready = False
        self.getImage(adress, name)
        self.loadImage()
        self.cropImage()
        self.greyImage()
        self.edgeDetectimage()
        self.loadImage()
        self.circleDetectImage()
        self.ids.rawimage.reload()
        self.ids.imgcurrent.reload()
        self.redDetectImage()
        self.ready = True
    def autotoggle(self, adress, name):
        #if self.ready == True:
        Clock.schedule_interval(lambda dt:self.auto(adress, name), 2)
        #time.sleep(2)
        #self.autotoggle(adress, name)
class MyScreenManager(ScreenManager):
    pass


class TouchCoordGetterImage(Image):
#    def on_mouse_pos(self, pos):
#        print(pos)
#    Window.bind(mouse_pos=on_mouse_pos)
    pass

class ImageProcessed(BoxLayout):
    pass




if __name__== '__main__':
    SmartApp().run()
