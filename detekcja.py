import cv2
import numpy as np
from matplotlib import pyplot as plt
import sympy as sp

def kolko():
    imgkol = cv2.imread('./images/edgeDetected/edgeDetected.jpg',0)
    imgkol2 = imgkol.copy()

    template = cv2.imread('./images/templates/zero.jpg',0)
    template2 = cv2.imread('./images/templates/dwa.jpg', 0)
    template4 = cv2.imread('./images/templates/cztery.jpg', 0)

    w, h = template.shape[::-1]
    w2, h2 = template2.shape[::-1]
    w4, h4 = template4.shape[::-1]

    imgkol = imgkol2.copy()

    res = cv2.matchTemplate(imgkol,template,cv2.TM_CCOEFF)
    res2 = cv2.matchTemplate(imgkol,template2,cv2.TM_CCOEFF)
    res4 = cv2.matchTemplate(imgkol,template4,cv2.TM_CCOEFF)

    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)
    min_val2, max_val2, min_loc2, max_loc2 = cv2.minMaxLoc(res2)
    min_val4, max_val4, min_loc4, max_loc4 = cv2.minMaxLoc(res4)

    top_left = max_loc
    top_left2 = max_loc2
    top_left4 = max_loc4

    bottom_right = (top_left[0] + w, top_left[1] + h)
    bottom_right2 = (top_left2[0] + w2, top_left2[1] + h2)
    bottom_right4 = (top_left4[0] + w4, top_left4[1] + h4)

    cv2.rectangle(imgkol,top_left, bottom_right, 255, 2)
    cv2.rectangle(imgkol,top_left2, bottom_right2, 255, 2)
    cv2.rectangle(imgkol,top_left4, bottom_right4, 255, 2)

    p1 = np.array([bottom_right[0], bottom_right[1], 0])
    p2 = np.array([(bottom_right2[0]+top_left2[0])*0.5, bottom_right2[1] , 0])
    p4 = np.array([top_left4[0], bottom_right4[1], 0])

    a = np.linalg.norm(p4 -p2)
    b = np.linalg.norm(p4 - p1)
    c = np.linalg.norm(p2 - p1)
    s = (a + b + c) / 2
    R = a*b*c / 4 / np.sqrt(s * (s - a) * (s - b) * (s - c))
    b1 = a*a * (b*b + c*c - a*a)
    b2 = b*b * (a*a + c*c - b*b)
    b3 = c*c * (a*a + b*b - c*c)
    P = np.column_stack((p1, p2, p4)).dot(np.hstack((b1, b2, b3)))
    P /= b1 + b2 + b3

    cx = int(np.round(P[0]))
    cy = int(np.round(P[1]))
    r = int(np.round(R))

    cv2.circle(imgkol, (cx, cy), r, (255, 0, 0), 2)

   # plt.subplot(111),plt.imshow(res,cmap = 'gray')
   # plt.title('Matching Result'), plt.xticks([]), plt.yticks([])
   # plt.subplot(121),plt.imshow(imgkol,cmap = 'gray')
   # plt.title('Detected Point'), plt.xticks([]), plt.yticks([])
   # plt.show()

    a1, b1 = np.polyfit((p1[0], cx), (p1[1], cy), 1)
    beta = np.rad2deg(np.arctan(a1))
    a2, b2 = np.polyfit((p4[0], cx), (p4[1], cy), 1)
    gamma = np.rad2deg(np.arctan(a2))
    gamma = 180+gamma #poprawienie do uwzglednienia zwrotu
    print(beta, a1, b1)
    print(gamma, a2, b2)
    #print(gamma)
    return beta, gamma
