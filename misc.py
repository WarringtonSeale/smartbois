import cv2
import numpy as np
from getImage import getImage
imgid = 'amper1'
imgtype = 'jpg'

getImage(imgid)
image = cv2.imread('./images/raw/'+ imgid + '.' + imgtype)

image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
height, width = image.shape[:2]
image = image[360:860, 1050:1720]
cv2.imwrite('./images/cropped/' + imgid + '.' + imgtype, image)
image = cv2.boxFilter(image, -1, (6, 6), (-1, -1))

image = cv2.Scharr(image, -1, 1, 0, 1, 4)
cv2.imwrite('./images/scharr/' + imgid + '.' +imgtype, image)
image = cv2.Canny(image, 225, 225, 4)

cv2.imshow('image', image)





cv2.waitKey(15000)
